//
//  AppDelegate.h
//  moduleTest
//
//  Created by goat on 2019/8/23.
//  Copyright © 2019 3Pomelos. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

